[Here's the relevant script](escape)

Suppose you want to pass information to a script for further processing. Take the following

```
pandoc $1.md
```

Calling your script as

```
script foo
```

will run the desired command:

```
pandoc foo.md
```

What about

```
script "file 'name'"
```

That won't work. You need to surround the argument in single quotes, but since the argument has single quotes inside it, that won't work either.

You need to take advantage of Bash concatenating consecutive strings automatically. `echo 'this'"thing"` will output `thisthing`.

That is what the [escape script](escape) does for you. It wraps the argument in single quotes and then "escapes" single quotes by doing a text replacement that converts single quotes to a closing single quote, double quoted single quote, and opening single quote.

```
echo 'this'"'"'thing'
```

returns

```
this'thing
```

This line `str2="${str//$'\''/$'\''$'\"'$'\''$'\"'$'\''}"` looks kind of goofy. I'm using [ANSI-C quoting](https://www.gnu.org/software/bash/manual/html_node/ANSI_002dC-Quoting.html) inside string replacement. `$'\''` is a different way to represent a single quote character. `$'\"'` is an alternative representation of a double quote. The rest is string replacement [as explained here](https://linuxhandbook.com/replace-string-bash/).
