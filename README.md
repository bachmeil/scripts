# scripts

Some scripts I use to simplify my life.

- [Convenience scripts for Pandoc that I use when teaching](pandoc-scripts.md)
- [A blog generator written in Bash (link to repo)](https://codeberg.org/bachmeil/bash-blog-generator)
- [Dealing with spaces and single quotes in Bash arguments](spaces-quotes.md)
- [Miscellaneous notes relevant to Bash scripting](cli-notes.md)
- [Things I've learned about having a second brain on the command line](cli-second-brain.md)
- [Moving forward with Vim usage](using-vim.md)