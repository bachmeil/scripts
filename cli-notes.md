# Various notes

To open a program in a new tab in my existing Mate terminal:

```
mate-terminal --tab -e 'vim file.txt'
```

In a new window:

```
mate-terminal --window -e 'vim file.txt'
```

To open Emacs in the terminal without an init file:

```
emacs -nw -q file.txt
```

To open Emacs in the terminal with an alternative init file:

```
emacs -nw -q -l custom.el file.txt
```

To navigate inside the terminal, with the ability to insert characters, use Ctrl-b (backward) and Ctrl-f (forward). 