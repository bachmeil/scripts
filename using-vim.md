# Using Vim

I have traditionally been an Emacs user. Not because I like it as a text editor (the defaults are absolutely terrible for no reason) but because it works so well for programming. Over time, my Emacs usage has declined. It's a hassle to constantly move my configuration files from machine to machine. Now that I use D for most of my programming, I'm more likely to whip up a command-line app written in D than to write Emacs Lisp functions.

Vim is always available for me. It has sensible default behavior. It doesn't have a clunky, weird feeling to it.

I have no idea how much I'll use Vim this year. I do know it will be more than I used it last year and any year before that.

The default that I hate in Vim is the lack of word wrapping. That might make sense for programming. If you're writing markdown, the default should be different. Put this in your .vimrc:

```
:set wrap
:set lbr
```

Now you have soft wrapping at word boundaries. A great way to write markdown documents.