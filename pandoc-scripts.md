# Pandoc Scripts

I frequently (particularly for teaching) create PDF documents using Pandoc. There are two main templates, [lecture.latex](lecture.latex) for lecture notes, and [gendoc.latex](gendoc.latex) for single-spaced handouts and other communications.

I put both of those templates in the Pandoc data directory, which for me as an Ubuntu user is `~/.local/share/pandoc/templates`. I then put [gendoc](gendoc) and [lecture](lecture) in `~/bin`, which is in my Bash PATH. Finally, I make both of them executable: `chmod a+x gendoc; chmod a+x lecture`.

With this one-minute setup out of the way, I can create a PDF file from a markdown document in any directory. Note that the `.md` extension on the filename is optional.

```
# For a single-spaced handout
gendoc 'markdown file.md'

# For my personal lecture notes
lecture 'markdown file.md'
```

The low mental overhead and reliability of these scripts is most valuable when I'm pressed for time, which seems to be twenty minutes before every class I teach.

If you clone this repo, you can install on Ubuntu (and probably other Linux distros) using:

```
mkdir -p $HOME/.local/share/pandoc/templates
cp lecture.latex $HOME/.local/share/pandoc/templates/
cp gendoc.latex $HOME/.local/share/pandoc/templates/
cp beamer.template $HOME/.local/share/pandoc/templates/
cp lecture $HOME/bin/
cp gendoc $HOME/bin/
cp beamer $HOME/bin/
chmod a+x $HOME/bin/lecture
chmod a+x $HOME/bin/gendoc
chmod a+x $HOME/bin/beamer
```
